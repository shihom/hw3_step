'''
Name: Shiho Matsuura
Execution: Calculator (+, -, *, /, decimals). Modularised.
'''

#reads number, returns token and index
def read_number(line, index):
    number = 0
    while index < len(line) and line[index].isdigit():
        number = number * 10 + int(line[index])
        index += 1
    if index < len(line) and line[index] == '.':
        index += 1
        digit = 0.1
        while index < len(line) and line[index].isdigit():
            number += int(line[index]) * digit
            digit *= 0.1
            index += 1
    token = {'type': 'NUMBER', 'number': number}
    return token, index


#reads operator, returns token dict and index of next char
def read_plus(index):
    token = {'type': 'ADD'}
    return token, index + 1


def read_minus(index):
    token = {'type': 'SUBTRACT'}
    return token, index + 1


def read_times(index):
    token = {'type': 'MULTIPLY'}
    return token, index + 1


def read_divided_by(index):
    token = {'type': 'DIVIDE'}
    return token, index + 1


def tokenize(input):
    tokens = [] #creates a list of dictionaries
    index = 0
    line = input.replace(" ", "") #removes spaces
    read_functions = {
        '+': read_plus,
        '-': read_minus,
        '*': read_times,
        '/': read_divided_by,
    } #creates a dict of functions to call
    #eg. if '+' is found, read_plus will be called
    while index < len(line):
        if line[index].isdigit():
            (token, index) = read_number(line, index)
        else:
            for op in read_functions.keys():
                if line[index] == op:
                    (token, index) = read_functions[op](index)
        tokens.append(token)
    return tokens


def evaluate1(tokens):
    temp = [] #temporary list for numbers/operators to be evaluated later.
    tokens.insert(0, {'type': 'ADD'}) # Insert a dummy '+' token
    index = 1
    while index < len(tokens):
        if tokens[index]['type'] == 'NUMBER':
            if tokens[index - 1]['type'] == 'ADD':
                temp.extend([tokens[index - 1], tokens[index]])
            elif tokens[index - 1]['type'] == 'SUBTRACT':
                temp.extend((tokens[index - 1], tokens[index]))
            elif tokens[index - 1]['type'] == 'MULTIPLY':
                new_num = tokens[index - 2]['number'] * tokens[index]['number']
                temp[-1] = {'type': 'NUMBER', 'number': new_num}
            elif tokens[index - 1]['type'] == 'DIVIDE':
                new_num = float(tokens[index - 2]['number']) / float(tokens[index]['number'])
                temp[-1] = {'type': 'NUMBER', 'number': new_num}
            else:
                print 'Invalid syntax'
        index += 1
    return temp


def evaluate2(tokens):
    answer = 0
    tokens.insert(0, {'type': 'ADD'}) # Insert a dummy '+' token
    index = 1
    while index < len(tokens):
        if tokens[index]['type'] == 'NUMBER':
            if tokens[index - 1]['type'] == 'ADD':
                answer += tokens[index]['number']
            elif tokens[index - 1]['type'] == 'SUBTRACT':
                answer -= tokens[index]['number']
            else:
                print 'Invalid syntax'
        index += 1
    return answer


def main():
    while True:
        print '> ',
        line = raw_input()
        tokens = tokenize(line)
        tokens2 = evaluate1(tokens)
        answer = evaluate2(tokens2)
        print "answer = %s\n" % round(answer, 2)

if __name__ == '__main__':
    main()
